function set(object, path, value = undefined) {
  let innerPath = path
  
  if (typeof path === 'string' && path) {
    if (~path.indexOf('.')) {
      innerPath = path.split('.')
    } else {
      innerPath = [ path ]
    }
  }
  
  if (Array.isArray(innerPath)) {
    innerPath.reduce((targetObject, pathPart, index, srcArray) => {
      if (typeof targetObject[pathPart] !== 'object') {
        targetObject[pathPart] = {}
      }
      if (index === srcArray.length - 1) {
        targetObject[pathPart] = value
      }
      return targetObject[pathPart]
    }, object)
  }
  return object
}

module.exports = set