module.exports = {
  get: require('./get'),
  set: require('./set'),
  pick: require('./pick'),
  mapValues: require('./mapValues'),
  escapeRegExp: require('./escapeRegExp')
}