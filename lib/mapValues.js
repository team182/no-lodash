function mapValues(object, handler) {
  return Object.keys(object).reduce((result, key) => (result[key] = handler(object[key], key, object), result), {})
}

module.exports = mapValues