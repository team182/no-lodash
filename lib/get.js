function get(object, path, dflt = undefined) {
  let result
  let innerPath = path
  
  if (typeof path === 'string' && path) {
    if (~path.indexOf('.')) {
      innerPath = path.split('.')
    } else {
      innerPath = [ path ]
    }
  }

  if (Array.isArray(innerPath)) {
    result = innerPath.reduce((source, pathPart) => {
      if (source !== undefined) {
        return source[pathPart]
      }
      return undefined
    }, object)
  }
  
  return result === undefined ? dflt : result
}

module.exports = get