const get = require('./get')
const set = require('./set')

function pick(object, paths) {
  return paths.reduce((result, path) => {
    const value = get(object, path)
    if (value !== undefined) {
      set(result, path, value)
    }
    return result
  }, {})
}

module.exports = pick